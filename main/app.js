function distance(first, second){
	if (!(first instanceof Array) || !(second instanceof Array)) {
		throw {message : 'InvalidType'};
	}

	if (first.length == 0 && second.length == 0) {
		return 0;
	}

	first = first.filter((element, index) => first.indexOf(element) === index);

	second = second.filter((element, index) => second.indexOf(element) === index);

	let nr = 0;
	first.forEach(element => {
		if (second.indexOf(element) === -1) {
			nr++;
		} else {
			second.splice(second.indexOf(element), 1);
		}
	});

	nr += second.length;

	return nr;
}


module.exports.distance = distance